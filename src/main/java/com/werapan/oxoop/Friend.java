/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;

/**
 *
 * @author black
 */
public class Friend implements Serializable{
    private String name;
    private String tel;
    private int age;
    private int id;
    private static int lastId = 1;
    
    public Friend(String name, int age, String tel){
        this.id = lastId++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }
    
    public int getId(){
        return id;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public int getAge(){
        return age;
    }
    
    public void setAge(int age) throws Exception{
        if(age<0){
            throw new Exception();
        }
        this.age = age;
    }
    
    public String getTel(){
        return tel;
    }
    
    public void setTel(){
        this.tel= tel ;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", tel=" + tel + ", age=" + age + ", id=" + id + '}';
    }

    public static void main(String[] args){
        Friend f = new Friend("Ron",30,"0258");
        try{
            f.setAge(-1);
        }catch (Exception ex) {
            System.out.println("Age is lower than 0!!");
        }
        System.out.print("" + f);
    }
}
